const admin = require('firebase-admin');
const functions = require('firebase-functions');
var nanoid = require('nanoid');
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());
admin.initializeApp(functions.config().firebase);

let db = admin.firestore();


const Url= db.collection('url');

function valid(url) {
  const expression = new RegExp(/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi)
  if(!expression.test(url)) return 0;
  return 1;
}
app.use(express.json())

app.get('/',(req,res) => {
  // urls = Url.findAll().then(function(users) {
  //   urls = users.map(user => user.dataValues.long)
  //   res.send(urls)
  //  })
  res.json({msg: 'POST to /api/shortner with "longurl" to use the service' });
})
app.post('/api/shortner', (req, res) => {
    let originalUrl = req.body.url
    if(!valid(originalUrl)){
        res.status(400).json({error: "Not a valid url"})
        return
    }
    let shortUrl = nanoid(5)
      Url.add({
        long: originalUrl,
        short: shortUrl
      }).then(result => {
        // console.log({shorturl: `${req.get('host')}/${shortUrl}`});
        return res.json({shorturl: `${shortUrl}`});
      }).catch(err => {
      res.json({error: err.message})
    }
      )})
app.get('/:surl', (req,res) => {
    if(req.params.surl === 'favicon.ico'){
        res.end()
        return
    }
    Url.where('short', '==', req.params.surl).limit(1).get().then(result => {
      // result.forEach(r => console.log(r))
      if (!result) {
        return res.status(404).end()
      }
      result.forEach(doc => {
        res.redirect(doc.data().long);
      });
      return 0;
    }).catch(err => {
    console.log('Error getting document', err);
    res.json({error: err.message})
  })
})

app.post('/secret/:slug', (req, res) => {
  let originalUrl = req.body.url
  if(!valid(originalUrl)) return res.status(400).json({error: "Not a valid url"});
  let shortUrl = req.params.slug
    let result = Url.add({
      long: originalUrl,
      short: shortUrl
    }).then(result => {
      return res.json({shorturl: `${shortUrl}`});
    }).catch(err => {
    res.json({error: err.message})
  })
})


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.shortner = functions.https.onRequest(app);
